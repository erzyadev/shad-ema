#include <vector>
#include <iostream>
#include <sstream>
#include <chrono>
#include <algorithm>
#include <random>
#include <cstdlib>
#include <cassert>
#include <cmath>

#include <cstdlib>
#include <cstdio>
#include <cstring>

#include <unordered_map>
#include "absl/container/flat_hash_map.h"
#include <tsl/hopscotch_map.h>

// TODO: include proper headers.

using int64 = long long;

struct PerfMetrics
{
    int64 CpuInstructions = 0;
    int64 CacheMisses = 0;
    int64 TimeNs = 0;
};

template<class HashMapType>
void measure(const HashMapType& hashMap, const std::vector<int>& queries, PerfMetrics* perfMetrics)
{
    // TODO: implement perf recording.

    int res = 0;

    for (auto q : queries) {
        auto it = hashMap.find(q);
        if (it != hashMap.end()) {
            res ^= it->second;
        }
    }

    if (res == 42) {
        std::cerr << "IGNORE" << std::endl;
    }
}
