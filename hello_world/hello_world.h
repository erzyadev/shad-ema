#pragma once

#include <stdexcept>
#include <string>

std::string hello_world() {
  throw std::runtime_error("no implementation");
}
