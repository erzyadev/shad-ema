#include "query.h"

#include <array>
#include <vector>
#include <iostream>
#include <sstream>
#include <chrono>
#include <algorithm>
#include <cstdlib>
#include <cassert>
#include <cmath>
#include <memory>

#include <cstdlib>
#include <cstdio>
#include <cstring>


class BTreeSearcher : public Searcher
{
public:
    BTreeSearcher(const std::vector<int64>& /*numbers*/)
    {
        // TODO: to be implemented
    }

    bool search(int64 query) override
    {
        // TODO: to be implemented
        return false;
    }
};

class VEBBinarySearcher : public Searcher
{
public:
    VEBBinarySearcher(const std::vector<int64>& numbers)
    {
        // TODO: to be implemented
    }

    bool search(int64 query) override
    {
        // TODO: to be implemented
        return false;
    }
};


int main(int argc, char** argv)
{
    assert(argc == 2);

    size_t n;
    int64 maxNumber;

    std::vector<int64> numbers;

    std::cin >> n >> maxNumber;
    numbers.resize(n);
    for (size_t i = 0; i < n; ++i) {
        std::cin >> numbers[i];
        assert(numbers[i] >= 0 && numbers[i] < maxNumber);
    }

    std::unique_ptr<Searcher> searcher;

    std::string mode = argv[1];

    if (mode == "stl") {
      searcher = std::make_unique<STLBinarySearcher>(numbers);
    } else if (mode == "btree") {
      // TODO: to be implemented
    } else if (mode == "veb") {
      // TODO: to be implemented
    } else {
      throw std::runtime_error("unknown mode");
    }

    std::cout << "READY" << std::endl;

    int64 queryCount, seed, multiplier, addend;
    std::cin >> queryCount >> seed >> multiplier >> addend;

    auto queryProcessor = QueryProcessor(maxNumber, seed, multiplier, addend);
    for (int index = 0; index < queryCount; ++index) {
        bool result = searcher->search(queryProcessor.next());
        queryProcessor.onQueryResult(result);
        /**
         * Uncomment to output in local runs
         */
        // std::cout << result;
        // if (index + 1 < queryCount) {
        //     std::cout << " ";
        // } else {
        //     std::cout << std::endl;
        // }
    }

    std::cout << queryProcessor.getResult() << std::endl;

    return 0;
}


