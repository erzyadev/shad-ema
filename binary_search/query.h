#pragma once

#include <vector>
#include <algorithm>

typedef long long int64;

class QueryProcessor
{
public:
    QueryProcessor(int64 maxNumber, int64 seed, int64 multiplier, int64 addend)
        : maxNumber_(maxNumber), seed_(seed), multiplier_(multiplier), addend_(addend), current_(seed), result_(seed + 1)
    { }

    int64 next()
    {
        int64 result = current_;
        current_ = ((current_ * multiplier_) % maxNumber_ + addend_) % maxNumber_;
        return result;
    }

    void onQueryResult(bool found)
    {
        result_ = ((result_ * ((found ? 1 : 0) + multiplier_)) % maxNumber_ + addend_) % maxNumber_;
    }

    int64 getResult()
    {
        return result_;
    }

private:
    int64 maxNumber_;
    int64 seed_;
    int64 multiplier_;
    int64 addend_;

    int64 current_;
    int64 result_;
};

class Searcher {
public:
    virtual bool search(int64 query) = 0;
    virtual ~Searcher() {}
};

class STLBinarySearcher : public Searcher
{
public:
    STLBinarySearcher(const std::vector<int64>& numbers)
        : numbers_(numbers)
    { }

    bool search(int64 query) override
    {
        return std::binary_search(numbers_.begin(), numbers_.end(), query);
    }

private:
    std::vector<int64> numbers_;
};
