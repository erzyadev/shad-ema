#include "solution.h"

void print_size(std::ostream& os, int size)
{
    std::vector<std::string> scale = {"B", "KB", "MB", "GB"};

    double fSize = size;

    int scaleIndex = 0;
    while (fSize > 1000) {
        ++scaleIndex;
        fSize /= 1000;
    }

    os << std::setprecision(3) << int(10 * fSize) / 10.0 << scale[scaleIndex] << std::setprecision(5);
}

int main(int argc, char** argv)
{
    int queryCount = 10000;
    for (int size = 512; size < 512 * 1024 * 1024; size = int(double(size) * 1.5)) {
        double averageAccessTimeNs = measure(size, queryCount);
        print_size(std::cout, size);
        std::cout << ": " << averageAccessTimeNs << std::endl;
    }

    return 0;
}

