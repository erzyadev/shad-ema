#include "solution.h"

void printResult(const PerfMetrics& perfMetrics, int queryCount)
{
    std::cout << "cpu_instrictions:" << static_cast<double>(perfMetrics.CpuInstructions) / queryCount << std::endl;
    std::cout << "cache_misses:" << static_cast<double>(perfMetrics.CacheMisses) / queryCount << std::endl;
    std::cout << "time:" << static_cast<double>(perfMetrics.TimeNs) / queryCount << std::endl;
}

void generateDataAndQueries(int n, int q, std::vector<std::pair<int, int>>* data, std::vector<int>* queries)
{
    std::random_device device;
    std::mt19937 rng(device());
    std::uniform_int_distribution<std::mt19937::result_type> distr(n, std::numeric_limits<int>::max());
    for (int i = 0; i < n; ++i) {
        data->push_back(std::make_pair(i, distr(rng)));
    }

    for (int i = 0; i < q; ++i) {
        if (i % 2 == 0) {
            queries->push_back(i % n);
        } else {
            queries->push_back(distr(rng));
        }
    } 
}

int main(int argc, char** argv)
{
    size_t N = std::atoi(argv[1]);
    size_t Q = std::atoi(argv[2]);

    std::vector<std::pair<int, int>> data;
    std::vector<int> queries;
    std::cerr << "Generating data..." << std::endl;
    generateDataAndQueries(N, Q, &data, &queries);
    std::cerr << "Generated" << std::endl;

    std::cerr << "Filling hash maps..." << std::endl;
    std::unordered_map<int, int> stlMap;
    absl::flat_hash_map<int, int> flatMap;
    tsl::hopscotch_map<int, int> hsMap;
    for (int i = 0; i < N; ++i) {
        stlMap.insert(data[i]);
        flatMap.insert(data[i]);
        hsMap.insert(data[i]);
    }
    std::cerr << "Filled" << std::endl;
    {
        std::cout << "=== STL map ===" << std::endl;
        PerfMetrics perfMetrics;
        measure(stlMap, queries, &perfMetrics);
        printResult(perfMetrics, queries.size());
        std::cout << std::endl;
    }

    {
        std::cout << "=== Abseil flat hash map ===" << std::endl;
        PerfMetrics perfMetrics;
        measure(flatMap, queries, &perfMetrics);
        printResult(perfMetrics, queries.size());
        std::cout << std::endl;
    }

    {
        std::cout << "=== Hopscotch map ===" << std::endl;
        PerfMetrics perfMetrics;
        measure(hsMap, queries, &perfMetrics);
        printResult(perfMetrics, queries.size());
        std::cout << std::endl;
    }

    return 0;
}

