#include "grader.h"

#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>

#include <iostream>
#include <string>
#include <vector>
#include <chrono>
#include <algorithm>
#include <random>
#include <numeric>

using namespace std;

struct Result
{
    double value;
    double variation;
};

std::ostream& operator<<(std::ostream& os, const Result& result)
{
    os << result.value << " +- " << result.variation;
    return os;
}


class EventTimer
{
public:
    void StartEvent()
    {
        StartTime_ = chrono::high_resolution_clock::now();
    }

    double StopEvent()
    {
        auto finishTime = chrono::high_resolution_clock::now();
        return chrono::duration_cast<chrono::nanoseconds>(finishTime - StartTime_).count() / 1000.0;
    }

private:
    chrono::time_point<chrono::high_resolution_clock> StartTime_;
    vector<long long> Timings_;
};


void checkSystemCallResult(long long result, const string& commandName)
{
    if (result < 0) {
        cerr << "Something goes wrong: '" << commandName << "' returns " << result << "; error: " << strerror(errno) << std::endl;
        exit(1);
    }
}


Result TestSequentialRead(const std::string& filename)
{
    // TO BE IMPLEMENTED

    // This is some sample (incorrect) implementation.
    int chunkSize = 1000 * 1000;
    std::vector<char> data(chunkSize);

    int fd = ::open(filename.c_str(), O_CREAT | O_WRONLY, S_IRUSR | S_IWUSR);
    checkSystemCallResult(fd, "open");

    for (int index = 0; index < 100; ++index) {
        ::write(fd, data.data(), chunkSize);
    }

    EventTimer timer;

    timer.StartEvent();
    ::lseek(fd, 0, SEEK_SET);
    for (int index = 0; index < 100; ++index) {
        ::read(fd, data.data(), chunkSize);
    }
    auto msecs = timer.StopEvent();

    return Result{110.0, 10.0};
}

Result TestSequentialWrite(const std::string& filename)
{
    // TO BE IMPLEMENTED
    return Result{90.0, 9.0};
}

Result TestRandomRead(const std::string& filename)
{
    // TO BE IMPLEMENTED

    // This is some sample (incorrect) implementation.
    int chunkSize = 1024 * 1024;
    std::vector<char> data(chunkSize);

    int fd = ::open(filename.c_str(), O_CREAT | O_WRONLY, S_IRUSR | S_IWUSR);
    checkSystemCallResult(fd, "open");

    for (int index = 0; index < 100; ++index) {
        ::write(fd, data.data(), chunkSize);
    }

    EventTimer timer;

    std::mt19937 generator(42);
    std::uniform_int_distribution<> distribution(0, 99 * chunkSize);

    timer.StartEvent();
    for (int index = 0; index < 100; ++index) {
        ::lseek(fd, distribution(generator), SEEK_SET);
        ::read(fd, data.data(), 1);
    }
    auto msecs = timer.StopEvent();
    return Result{61000, 1100};
}

Result TestRandomWrite(const std::string& filename)
{
    // TO BE IMPLEMENTED
    return Result{8100, 1200};
}

int main(int argc, char** argv) {
    if (argc < 3) {
        cerr << "Incorrect number of arguments: " << argc << ", expected at least 3 arguments: ./" << argv[0] << " {mode} {filename}" << endl;
        return 1;
    }

    string mode = argv[1];
    string filename = argv[2];
    if (mode == "seq-read") {
        auto result = TestSequentialRead(filename);
        cout << result << " MB/s" << endl;
    } else if (mode == "seq-write") {
        auto result = TestSequentialWrite(filename);
        cout << result << " MB/s" << endl;
    } else if (mode == "rnd-read") {
        auto result = TestRandomRead(filename);
        cout << result << " mcs" << endl;
    } else if (mode == "rnd-write") {
        auto result = TestRandomWrite(filename);
        cout << result << " mcs" << endl;
    } else {
        cerr << "Incorrect mode: " << mode << endl;
        return 1;
    }

    return 0;
}
